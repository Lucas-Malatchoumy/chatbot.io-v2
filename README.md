# Chatbot.io-V

## Installation
Clone my project.

```bash
git clone https://gitlab.com/Lucas-Malatchoumy/chatbot.io-v2.git
```

Use the package manager [npm](https://www.npmjs.com/) to install socket.io, socket.io-client, and express.

```bash
nvm install socket.io
```
```bash
nvm install express
```
```bash
nvm i socket.io-client
```

Use node v14 use :
```bash
nvm install 14
```
## Configuration
Replace in /server/server.js your localhost port for running application :

```bash
const io = new Server(server, {
  cors: {
    origin: 'localhost',
    methods: ['GET', 'POST']
  }
});
```

Replace in /src/Chat.js and /src/index.js your localhost port to connect you to your server :

```bash
const io = require('socket.io-client');

const socket = io.connect('http://127.0.0.1:3000');
socket.on('connection', () => {
  // eslint-disable-next-line no-console
  console.log(socket.id);
});
```
### Usage

Start the application dev with :

```bash
npm run start
```
Start the server application in /src/server with :

```bash
node server.js
```
First of all, you have to enter necesserrly a username. You can also choose an avatar
After you will be in the chat

You can right different commands and a bot will send you a response : 

* Enter "hi" or "Hi" or "Hello" or "hello" and the three bots will say Hello to you
* Enter "HP" and a character name and Voldemort will show you characters face
* Enter "meteo" and a city and Mister Météo will give you the temp
* Enter "Hour" or "hour" and Mister Météo will give you the hourEnter "Help" or "help" and Helper will show you the commands
* Enter a calcul and Helper will giv you the result
* Enter "Activity for" and a number of participant and Voldemort wil give you an activity
* Enter "Attack" and Voldemort will put a spell on you
* Enter "clothes" and a city and Mister Météo will advise you on clothing
* Enter "creator" and Helper will send you the name of the application creator

