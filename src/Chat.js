const io = require('socket.io-client');

const socket = io.connect('http://127.0.0.1:3000');
socket.on('connection', () => {
  // eslint-disable-next-line no-console
  console.log(socket.id);
});

const Chat = class Chat {
  constructor() {
    this.el = document.querySelector('#app');
    this.elMessage = document.querySelector('.messages');
    localStorage.setItem('messages', '[]');
    this.lastMessage = '';
  }

  renderHeader() {
    return `
        <div class="row col-12">
            <nav class="navbar navbar-dark bg-dark">
                <div class="container-fluid">
                    <span class="navbar-brand mb-5 h1">ChatBot-V2</span>
                </div>
            </nav>
        </div>`;
  }

  renderContact(contact) {
    const { name, avatar } = contact;

    return `
        <div class="row mt-3">
            <div class="contact">
                <img class="avatar" src="${avatar}" class="rounded-circle" class="img-fluid">
                <div class="text">
                    <h6> ${name}</h6>
                </div>
            </div>
        </div>
        <hr class="hr">
        `;
  }

  renderContacts(contacts, user) {
    contacts.push(user);
    return `
        <div id ="contacts" class="col-3">
            ${contacts.map((contact) => this.renderContact(contact)).join('')}
        </div>`;
  }

  renderMessageSend(message) {
    socket.emit('fisrtMessage', message);
    const date = new Date();
    const { author, avatar, text } = message;
    return `
        <div class="messages-send row">
            <div class="col-6"></div>
            <div class="col-6">
                <div class="card mb-2 text-dark">
                    <h5 class="card-header">
                    <img class="avatar" src="${avatar}" class="rounded-circle" class="img-fluid">
                    <span>${author}</span>
                    </h5>
                    <div class="card-body">
                    <p class="card-text">${text}</p>
                    </div>
                    <div class="card-footer text-muted">
                    ${date.getHours()} : ${date.getMinutes()}
                    </div>
                </div>
            </div>
        </div>
        `;
  }

  receiveUserMessages() {
    socket.on('messageToAll', (message) => {
      this.elMessage.innerHTML += this.renderMessageReceived(message);
    });
  }

  receiveBotMessages() {
    socket.on('messageBotToAll', (message) => {
      this.elMessage.innerHTML += this.renderMessageReceived(message);
      this.elMessage.scrollTop = this.elMessage.scrollHeight - this.elMessage.clientHeight;
    });
  }

  renderMessageReceived(message) {
    const date = new Date();
    const { author, avatar, text } = message;
    return `
        <div class="messages-received row">
            <div class="col-6">
                <div class="card mb-2 text-dark">
                    <h5 class="card-header">
                    <img class="avatar" src="${avatar}" class="rounded-circle" class="img-fluid">
                    <span>${author}</span>
                    </h5>
                    <div class="card-body">
                    <p class="card-text">${text}</p>
                    </div>
                    <div class="card-footer text-muted">
                    ${date.getHours()} : ${date.getMinutes()}
                    </div>
                </div>
            </div>
        </div>
        `;
  }

  renderMessages() {
    return `<div class="col-9 mt-4">
            <div class="messages">
            </div>
            ${this.renderTypingMessage()}
        </div>
        `;
  }

  renderTypingMessage() {
    return `<div class="row">
            <div class="col-9">
                <div class="input-group mb-3">
                    <input id="input" type="text" class="form-control" placeholder="Message" aria-label="Message">
                </div>
            </div>
            <div class="col-3">
                <div class="d-grid gap-2">
                    <button id="btn" class="btn btn-primary" type="submit"> Envoyer</button>
                </div>
            </div>
        </div>
        `;
  }

  TypingMessage(user) {
    const el = document.querySelector('#input');
    const btnEl = document.querySelector('#btn');
    this.elMessage = document.querySelector('.messages');
    el.addEventListener('keydown', (e) => {
      if (e.code === 'Enter') {
        const text = el.value;
        const message = {
          author: user.name,
          avatar: user.avatar,
          text
        };
        if (el.value !== '') {
          this.setHistoryMessages(message);
          this.elMessage.innerHTML += this.renderMessageSend(message);
          this.lastMessage = this.getHistoryMessages().pop().text;
          // eslint-disable-next-line no-console
          console.log(this.lastMessage);
          el.value = '';
        }
      }
    });
    btnEl.addEventListener('click', () => {
      const text = el.value;
      const message = {
        author: user.name,
        avatar: user.avatar,
        text
      };
      if (el.value !== '') {
        this.setHistoryMessages(message);
        this.elMessage.innerHTML += this.renderMessageSend(message);
        this.lastMessage = this.getHistoryMessages().pop().text;
        // eslint-disable-next-line no-console
        console.log(this.lastMessage);
        el.value = '';
      }
    });
  }

  getHistoryMessages() {
    const message = JSON.parse(localStorage.getItem('messages'));
    // eslint-disable-next-line no-console
    console.log(message);
    return message;
  }

  setHistoryMessages(message) {
    const messages = this.getHistoryMessages();
    messages.push(message);
    localStorage.setItem('messages', JSON.stringify(messages));
  }

  run(contacts, user) {
    this.el.innerHTML += this.renderHeader();
    this.el.innerHTML += this.renderContacts(contacts, user);
    this.el.innerHTML += this.renderMessages();
    this.TypingMessage(user);
    this.receiveUserMessages();
    this.receiveBotMessages();
    this.elMessage.scrollTop = this.elMessage.scrollHeight - this.elMessage.clientHeight;
  }
};
export default Chat;
