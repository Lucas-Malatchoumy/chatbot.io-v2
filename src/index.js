import './index.scss';
import Chat from './Chat';
import Bot from './Bot';

const chat = new Chat();
const bots = [
  {
    name: 'Mister Météo',
    avatar: 'https://previews.123rf.com/images/studiobarcelona/studiobarcelona1605/studiobarcelona160500031/56783002-%ED%83%9C%EC%96%91-%EC%95%84%EC%9D%B4%EC%BD%98-.jpg?fj=1',
    actions:
      [{
        actionName: 'Meteo',
        actionExec: ['meteo'],
        action: async (message3) => {
        // eslint-disable-next-line no-console
          console.log(message3);
          const city = message3.replace('meteo ', '');
          // eslint-disable-next-line no-console
          console.log(city);
          return fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=ccf40dbab050b4955a1319c19855a723&units=metric`)
            .then((res) => res.json())
            .then((data) => {
              const text = `The temperature in ${city} is ${data.main.temp}`;
              // eslint-disable-next-line no-console
              // console.log(text);
              return text;
            });
        }
      },
      {
        actionName: 'Clothes',
        actionExec: ['clothes'],
        action: async (message3) => {
          const city = message3.replace('clothes ', '');
          return fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=ccf40dbab050b4955a1319c19855a723&units=metric`)
            .then((res) => res.json())
            .then((data) => {
              let text = '';
              if (data.main.temp <= 5) {
                text = `You must put on a scarf, a hat, and gloves! The temperature is ${data.main.temp}`;
              } else if (data.main.temp > 5 && data.main.temp <= 15) {
                text = `A small jacket will suffice! The temperature is ${data.main.temp}`;
              } else {
                text = `You can take out the sunglasses, put on shorts and a t-shirt! The temperature is ${data.main.temp}`;
              }
              return text;
            });
        }
      },
      {
        actionName: 'Say Hello',
        actionExec: ['Hi', 'hi', 'Hello', 'hello'],
        action: async () => {
          const text = 'Hello guy !';
          return text;
        }
      },
      {
        actionName: 'Hour',
        actionExec: ['Hour', 'hour'],
        action: async () => {
          const date = new Date();
          const text = `${date.getHours()} : ${date.getMinutes()}`;
          return text;
        }
      }]
  },
  {
    name: 'Voldemort',
    avatar: 'https://www.francetvinfo.fr/pictures/_C9o4O-ctp1_f3gwzfjNgufXVss/1200x900/2019/04/11/voldemortsipa.jpg',
    actions:
      [{
        actionName: 'Hp',
        actionExec: ['HP'],
        action: async (lastMessage) => {
          const character = lastMessage.replace('HP ', '');
          return fetch('http://hp-api.herokuapp.com/api/characters')
            .then((res) => res.json())
            .then((data) => {
              let text = '';
              data.forEach((element) => {
                if (character === element.name) {
                  text = `<img id="img" src="${element.image}" alt="">`;
                }
              });
              return text;
            });
        }
      },
      {
        actionName: 'spells',
        actionExec: ['Attack'],
        action: async () => {
          const spells = ['Avada Kedavra', 'Impero', 'Endoloris', 'Sectum Sempra'];
          const text = spells[Math.floor(Math.random() * spells.length)];
          return text;
        }
      },
      {
        actionName: 'Activity',
        actionExec: ['Activity For'],
        action: async (lastMessage) => {
          const participant = lastMessage.replace('Activity For ', '');
          return fetch(`http://www.boredapi.com/api/activity?participants=${participant}`)
            .then((res) => res.json())
            .then((data) => {
              const text = data.activity;
              return text;
            });
        }
      },
      {
        actionName: 'Say Hello',
        actionExec: ['Hi', 'hi', 'Hello', 'hello'],
        action: async () => {
          const text = 'Hello Mudblood !';
          return text;
        }
      }]

  },
  {
    name: 'Helper',
    avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/VisualEditor_-_Icon_-_Help.svg/1200px-VisualEditor_-_Icon_-_Help.svg.png',
    actions:
    [{
      actionName: 'Say Hello',
      actionExec: ['Hi', 'hi', 'Hello', 'hello'],
      action: async () => {
        const text = 'Hello guy !';
        return text;
      }
    },
    {
      actionName: 'Calculate',
      actionExec: ['+', '-', '*', '/'],
      action: async (lastMessage) => {
        let text = '';
        const signes = ['+ ', '- ', '* ', '/ '];
        for (let i = 0; i < signes.length; i += 1) {
          if (lastMessage.includes(signes[i])) {
            const finalSigne = signes[i];
            const remove = lastMessage.replace(signes[i], '');
            const numbers = remove.replace(/ /g, ' ');
            const finalNumbers = numbers.split(' ');
            for (let x = 0; x < finalNumbers.length; x += 1) {
              const nbr1 = parseFloat(finalNumbers[0]);
              const nbr2 = parseFloat(finalNumbers[1]);
              switch (finalSigne) {
                case '+ ':
                  text = nbr1 + nbr2;
                  break;
                case '- ':
                  text = nbr1 + nbr2;
                  break;
                case '* ':
                  text = nbr1 * nbr2;
                  break;
                case '/ ':
                  if (nbr2 !== 0) {
                    text = nbr1 / nbr2;
                  }
                  text = 'ERROR';
                  break;
                default:
                  text = 'nbr1 % nbr2';
                  break;
              }
            }
          }
        }
        return text;
      }
    },
    {
      actionName: 'Help',
      actionExec: ['Help', 'help'],
      action: async () => {
        const text = `
        <ul> <li>Enter "hi" or "Hi" or "Hello" or "hello" and the three bots will say Hello to you</li>
        <li>Enter "HP" and a character name and Voldemort will show you characters face</li>
        <li>Enter "meteo" and a city and Mister Météo will give you the temp</li>
        <li>Enter "Hour" or "hour" and Mister Météo will give you the hour</li>
        <li>Enter "Help" or "help" and Helper will show you the commands</li>
        <li>Enter a calcul with to parameters and Helper will giv you the result</li>
        <li>Enter "Activity for" and a number of participant and Voldemort wil give you an activity</li>
        <li>Enter "Attack" and Voldemort will put a spell on you</li>
        <li>Enter "clothes" and a city and Mister Météo will advise you on clothing</li>
        <li>Enter "creator" and Helper will send you the name of the application creator</li></ul>
        `;
        return text;
      }
    },
    {
      actionName: 'creator',
      actionExec: ['creator'],
      action: async () => {
        const text = 'The creator of this app is Lucas Malatchoumy';
        return text;
      }
    }
    ]
  }
];
const botsarray = bots.map((bot) => new Bot(bot));
const contacts = bots;
const connexion = document.getElementById('connexion');
const btn = document.getElementById('submit');
const userName = document.getElementById('userName');
const avatar2 = document.getElementById('avatar2');
function check(lastMessage) {
  botsarray.forEach((bot) => {
    const io = require('socket.io-client');
    const socket = io.connect('http://127.0.0.1:3000');
    socket.on('connection', () => {
      // eslint-disable-next-line no-console
      // console.log(socket.id);
    });
    bot.entities.actions.forEach((element) => {
      element.actionExec.forEach((cmd) => {
        if (lastMessage.includes(cmd)) {
          element.action(lastMessage).then((message) => {
            const messageF = {
              author: bot.entities.name,
              avatar: bot.entities.avatar,
              text: message
            };
            socket.emit('messageBot', messageF);
          });
        }
      });
    });
  });
}
function access() {
  connexion.remove();
  const user = { name: userName.value, avatar: avatar2.value };
  chat.run(contacts, user);
  const el = document.querySelector('#input');
  el.addEventListener('keydown', (e) => {
    if (e.code === 'Enter') {
      let lastMessage = chat.getHistoryMessages().pop().text;
      check(lastMessage);
      chat.elMessage.scrollTop = chat.elMessage.scrollHeight - chat.elMessage.clientHeight;
      lastMessage = '';
    }
  });
  const btnEl = document.querySelector('#btn');
  btnEl.addEventListener('click', () => {
    let lastMessage = chat.getHistoryMessages().pop().text;
    check(lastMessage);
    lastMessage = '';
  });
}
btn.addEventListener('click', () => {
  if (userName.value !== '') {
    access();
  } else {
    userName.placeholder = 'Please enter a username';
  }
});
