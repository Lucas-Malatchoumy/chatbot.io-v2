const express = require('express');

const app = express();
const http = require('http');

const server = http.createServer(app);
const { Server } = require('socket.io');

const io = new Server(server, {
  cors: {
    origin: 'http://127.0.0.1:9090',
    methods: ['GET', 'POST']
  }
});

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

io.on('connection', (socket) => {
  socket.on('fisrtMessage', (message) => {
    socket.broadcast.emit('messageToAll', message);
  });
  socket.on('messageBot', (messageF) => {
    socket.broadcast.emit('messageBotToAll', messageF);
  });
});

server.listen(3000, '0.0.0.0', () => {
  // eslint-disable-next-line no-console
  console.log('listening on *:3000');
});
